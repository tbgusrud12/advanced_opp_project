package GUIPromgramming;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class calculateCAI {
	public calculateCAI(){
		JFrame f = new JFrame();
		f.setSize(500, 200);
		
		JPanel inputNitrogenDioxide = new JPanel();
		inputNitrogenDioxide.setLayout(new GridLayout(2, 0));
		inputNitrogenDioxide.add(new JLabel("이산화탄소"));
		inputNitrogenDioxide.add(new JTextField(3));
		JPanel inputOzoneConcentration = new JPanel();
		inputOzoneConcentration.setLayout(new GridLayout(2, 0));
		inputOzoneConcentration.add(new JLabel("오존농도"));
		inputOzoneConcentration.add(new JTextField(3));
		JPanel inputCarbonDioxide = new JPanel();
		inputCarbonDioxide.setLayout(new GridLayout(2, 0));
		inputCarbonDioxide.add(new JLabel("이산화탄소"));
		inputCarbonDioxide.add(new JTextField(3));
		JPanel inputSulfurousAcidGas = new JPanel();
		inputSulfurousAcidGas.setLayout(new GridLayout(2, 0));
		inputSulfurousAcidGas.add(new JLabel("아황산가스"));
		inputSulfurousAcidGas.add(new JTextField(3));
		JPanel inputFineDust = new JPanel();
		inputFineDust.setLayout(new GridLayout(2, 0));
		inputFineDust.add(new JLabel("미세먼지"));
		inputFineDust.add(new JTextField(3));
		JPanel inputFineParticulateMatter = new JPanel();
		inputFineParticulateMatter.setLayout(new GridLayout(2, 0));
		inputFineParticulateMatter.add(new JLabel("초미세먼지"));
		inputFineParticulateMatter.add(new JTextField(3));
		
		JPanel input = new JPanel();
		input.add(inputNitrogenDioxide);
		input.add(inputOzoneConcentration);
		input.add(inputCarbonDioxide);
		input.add(inputSulfurousAcidGas);
		input.add(inputFineDust);
		input.add(inputFineParticulateMatter);
		input.add(new JButton("CAI조회"));
		
		JPanel result = new JPanel();
		JLabel resultMessage = new JLabel("입력한 값들로 측정한 통합대기환경지수(CAI) : ");
		result.add(resultMessage);

		f.add(input, BorderLayout.NORTH);
		f.add(result, BorderLayout.CENTER);
		f.setVisible(true);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public static void main(String args[]) {
		new calculateCAI();
	}
}
