package GUIPromgramming;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class MainFrame implements ActionListener{
	JFrame f;
	JMenuBar mb;
	JMenu Inquiry, File, Help, Admin, Exit;
	JMenuItem informationInquiry, calculateCAI, Import, New, Save, Export, helpHelp, adminAdmin, exitExit;
	
	
	MainFrame(){
		f = new JFrame("TEST");
		
		informationInquiry = new JMenuItem("기간별 오염통계 정보 조회");
		calculateCAI  = new JMenuItem("CAI 계산");
		Import  = new JMenuItem("Import");
		New  = new JMenuItem("New");
		Save  = new JMenuItem("Save");
		Export  = new JMenuItem("Export");
		helpHelp  = new JMenuItem("Help");
		adminAdmin  = new JMenuItem("Admin");
		exitExit = new JMenuItem("Exit");
		
		informationInquiry.addActionListener(this);
		calculateCAI.addActionListener(this);
		Import.addActionListener(this);
		New.addActionListener(this);
		Save.addActionListener(this);
		Export.addActionListener(this);
		helpHelp.addActionListener(this);
		adminAdmin.addActionListener(this);
		exitExit.addActionListener(this);
		
		mb = new JMenuBar();
		Inquiry = new JMenu("Inquiry");
		File = new JMenu("File");
		Help = new JMenu("Help");
		Admin = new JMenu("Admin");
		Exit = new JMenu("Exit");
		
		Inquiry.add(informationInquiry);
		Inquiry.add(calculateCAI);
		File.add(Import);
		File.add(New);
		File.add(Save);
		File.add(Export);
		Help.add(helpHelp);
		Admin.add(adminAdmin);
		Exit.add(exitExit);
		mb.add(Inquiry);
		mb.add(File);
		mb.add(Help);
		mb.add(Admin);
		mb.add(Exit);
		
		f.add(mb);
		f.setJMenuBar(mb);
		f.setLayout(null);
		f.setSize(400,400);
		f.setVisible(true);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == informationInquiry) {
			new InformationInquiry();
		}
		if(e.getSource() == calculateCAI) {
			new calculateCAI();
		}
		if(e.getSource() == Import) {
			
		}
		if(e.getSource() == New) {
			new MainFrame();
		}
		if(e.getSource() == Save) {
		}
		if(e.getSource() == Export) {
		}
		if(e.getSource() == helpHelp) {
		}
		if(e.getSource() == adminAdmin) {
		}
		if(e.getSource() == exitExit) {
			System.exit(0);
		}	
	}
	
	public static void main(String args[]) {
		new MainFrame();
	}
	
}
