package GUIPromgramming;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

public class GraphicExample2 {
	// public static void main(String args[]) {//이게 합친 버전
	public GraphicExample2() {
		JFrame frame = new JFrame("성적 그래프 프로그램");
		frame.setLocation(500, 200);
		frame.setPreferredSize(new Dimension(700, 450));
		JScrollPane scroll = new JScrollPane();
		Container contentPane = frame.getContentPane();

		SpringLayout layout = new SpringLayout();
		DrawingPanel drawingPanel = new DrawingPanel();
		drawingPanel.setLayout(layout);
		contentPane.setLayout(new BorderLayout());
		// 그래프를 그릴 패널

		JPanel controlPanel = new JPanel();
		JTextField text1 = new JTextField(3);
		JTextField text2 = new JTextField(3);
		JTextField text3 = new JTextField(3);
		JTextField text4 = new JTextField(3);
		JTextField text5 = new JTextField(3);
		JTextField text6 = new JTextField(3);
		JButton button = new JButton("그래프 그리기");
		controlPanel.add(new JLabel("이산화질소"));
		controlPanel.add(text1);
		controlPanel.add(new JLabel("오존농도"));
		controlPanel.add(text2);
		controlPanel.add(new JLabel("이산화탄소"));
		controlPanel.add(text3);
		controlPanel.add(new JLabel("아황산가스"));
		controlPanel.add(text4);
		controlPanel.add(new JLabel("미세먼지"));
		controlPanel.add(text5);
		controlPanel.add(new JLabel("초미세먼지"));
		controlPanel.add(text6);
		controlPanel.add(button);
		drawingPanel.setPreferredSize(new Dimension(15000, 150));
		scroll.setPreferredSize(new Dimension(500, 500));
		scroll.setViewportView(drawingPanel);
		contentPane.add(scroll);
		contentPane.add(controlPanel, BorderLayout.SOUTH);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		button.addActionListener(new DrawActionListener(text1, text2, text3, text4, text5, text6, drawingPanel));
		// "그래프 그리기" 버튼을 눌렀을때 작동 할 리스터등록
		frame.pack();
		frame.setVisible(true);
	}
}

//그래피를 그리는 패널 클래스
class DrawingPanel extends JPanel {
	double[] AirPollutionElement = new double[6];
	// 조회된 날짜 사이의 간격을 나타내는 dateTerm 변수를 통해 몇개의 그래프를 그릴것인지 확인한다.
	int dateTerm = calculateDate(1, 10) - calculateDate(1, 5) + 1;

	public void paint(Graphics g) {
		g.clearRect(0, 0, getWidth(), getHeight());
		g.drawLine(50, 250, dateTerm * 550, 250);
		for (int cnt = 1; cnt < 11; cnt++) {
			g.drawString(cnt * 10 + "", 25, 255 - 20 * cnt);
			g.drawLine(50, 250 - 20 * cnt, dateTerm * 550, 250 - 20 * cnt);
		}
		g.drawLine(50, 20, 50, 250);
		for (int i = 0; i < dateTerm; i++) {
			g.setColor(Color.BLACK);
			// g.drawString(날짜);
			g.drawString("이산화질소", i * 400 + 60, 270);
			g.drawString("오존농도", i * 400 + 120, 270);
			g.drawString("이산화탄소", i * 400 + 180, 270);
			g.drawString("아황산가스", i * 400 + 240, 270);
			g.drawString("미세먼지", i * 400 + 300, 270);
			g.drawString("초미세먼지", i * 400 + 360, 270);
			
			if(AirPollutionElement[0] * 666.6 < 30) g.setColor(Color.BLUE);
			else if(AirPollutionElement[0] * 666.6 < 60) g.setColor(Color.GREEN);
			else if(AirPollutionElement[0] * 666.6 < 80) g.setColor(Color.ORANGE);
			else if(AirPollutionElement[0] * 666.6 <= 100) g.setColor(Color.RED);
			if (AirPollutionElement[0] > 0)
				g.fillRect(i * 400 + 75, 250 - (int) (AirPollutionElement[0] * 666.6) * 2, 30,
						(int) (AirPollutionElement[0] * 666.6) * 2);
			
			if(AirPollutionElement[1] * 400 < 30) g.setColor(Color.BLUE);
			else if(AirPollutionElement[1] * 400 < 60) g.setColor(Color.GREEN);
			else if(AirPollutionElement[1] * 400 < 80) g.setColor(Color.ORANGE);
			else if(AirPollutionElement[1] * 400 <= 100) g.setColor(Color.RED);
			if (AirPollutionElement[1] > 0)
				g.fillRect(i * 400 + 135, 250 - (int) (AirPollutionElement[1] * 400) * 2, 30,
						(int) (AirPollutionElement[1] * 400) * 2);

			if(AirPollutionElement[2] * 33 < 30) g.setColor(Color.BLUE);
			else if(AirPollutionElement[2] * 33 < 60) g.setColor(Color.GREEN);
			else if(AirPollutionElement[2] * 33 < 80) g.setColor(Color.ORANGE);
			else if(AirPollutionElement[2] * 33 <= 100) g.setColor(Color.RED);
			if (AirPollutionElement[2] > 0)
				g.fillRect(i * 400 + 195, 250 - (int) (AirPollutionElement[2] * 33) * 2, 30,
						(int) (AirPollutionElement[2] * 33) * 2);

			if(AirPollutionElement[3] * 666.6 < 30) g.setColor(Color.BLUE);
			else if(AirPollutionElement[3] * 666.6 < 60) g.setColor(Color.GREEN);
			else if(AirPollutionElement[3] * 666.6 < 80) g.setColor(Color.ORANGE);
			else if(AirPollutionElement[3] * 666.6 <= 100) g.setColor(Color.RED);
			if (AirPollutionElement[3] > 0)
				g.fillRect(i * 400 + 255, 250 - (int) (AirPollutionElement[3] * 666.6) * 2, 30,
						(int) (AirPollutionElement[3] * 666.6) * 2);

			if(AirPollutionElement[4] * 0.5 < 30) g.setColor(Color.BLUE);
			else if(AirPollutionElement[4] * 0.5 < 60) g.setColor(Color.GREEN);
			else if(AirPollutionElement[4] * 0.5 < 80) g.setColor(Color.ORANGE);
			else if(AirPollutionElement[4] * 0.5 <= 100) g.setColor(Color.RED);
			if (AirPollutionElement[4] > 0)
				g.fillRect(i * 400 + 315, 250 - (int) (AirPollutionElement[4] * 0.5) * 2, 30,
						(int) (AirPollutionElement[4] * 0.5) * 2);

			if(AirPollutionElement[5] * 0.66 < 30) g.setColor(Color.BLUE);
			else if(AirPollutionElement[5] * 0.66 < 60) g.setColor(Color.GREEN);
			else if(AirPollutionElement[5] * 0.66 < 80) g.setColor(Color.ORANGE);
			else if(AirPollutionElement[5] * 0.66 <= 100) g.setColor(Color.RED);
			if (AirPollutionElement[5] > 0)
				g.fillRect(i * 400 + 375, 250 - (int) (AirPollutionElement[5] * 0.66) * 2, 30,
						(int) (AirPollutionElement[5] * 0.66) * 2);
		}
	}

//	public Point getToolTipLocation(MouseEvent event) {
//		return new Point(10, 20);
//	}

	public int calculateDate(int month, int day) {
		dateTerm = 0;
		Integer[] lastDays = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		for (int i = 0; i < month - 1; i++) {
			dateTerm += (i + 1) * lastDays[i];
		}
		dateTerm += day;
		return dateTerm;
	}

	void setScores(double NitrogenDioxide, double OzoneConcentration, double CarbonDioxide, double SulfurousAcidGas,
			double FineDust, double FineParticulateMatter) {
		this.AirPollutionElement[0] = NitrogenDioxide;
		this.AirPollutionElement[1] = OzoneConcentration;
		this.AirPollutionElement[2] = CarbonDioxide;
		this.AirPollutionElement[3] = SulfurousAcidGas;
		this.AirPollutionElement[4] = FineDust;
		this.AirPollutionElement[5] = FineParticulateMatter;
	}
}

//버튼 눌렀을때 동작하는 리스너
class DrawActionListener implements ActionListener {
	JTextField text1, text2, text3, text4, text5, text6;
	DrawingPanel drawingPanel;

	DrawActionListener(JTextField text1, JTextField text2, JTextField text3, JTextField text4, JTextField text5,
			JTextField text6, DrawingPanel drawingPanel) {
		this.text1 = text1;
		this.text2 = text2;
		this.text3 = text3;
		this.text4 = text4;
		this.text5 = text5;
		this.text6 = text6;
		this.drawingPanel = drawingPanel;
	}

	public void actionPerformed(ActionEvent e) {
		try {
			double NitrogenDioxide = Double.parseDouble(text1.getText());
			double OzoneConcentration = Double.parseDouble(text2.getText());
			double CarbonDioxide = Double.parseDouble(text3.getText());
			double SulfurousAcidGas = Double.parseDouble(text4.getText());
			double FineDust = Double.parseDouble(text5.getText());
			double FineParticulateMatter = Double.parseDouble(text6.getText());
			drawingPanel.setScores(NitrogenDioxide, OzoneConcentration, CarbonDioxide, SulfurousAcidGas, FineDust,
					FineParticulateMatter);
			drawingPanel.repaint();
		} catch (NumberFormatException nfe) {
			JOptionPane.showMessageDialog(drawingPanel, "잘못된 숫자 입력입니다", "에러메시지", JOptionPane.ERROR_MESSAGE);
		}
	}
}
