package GUIPromgramming;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.border.Border;

public class InformationInquiry { // 날짜 두개 + 날짜에 다른 조회소 콤보박스생성 + 변수 정리 + radioButton생성 +radioButton 선택사항 인지 +
						// 그래프그리기 도전!
	JFrame frm = new JFrame("기간별 오염통계 정보 조회");
	JPanel pnlNorth = new JPanel();
	Integer[] inquiryData = new Integer[4];
	String inquiryMeasureData;
	Integer[] monthsList = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
	Integer[] days28 = new Integer[28]; // 그에따라 동작될 버튼
	Integer[] days30 = new Integer[30];
	Integer[] days31 = new Integer[31];
	JComboBox<Integer> firstMonth = new JComboBox<>(monthsList);
	JComboBox<Integer> firstDay = new JComboBox<>(days31);
	JComboBox<Integer> secondMonth = new JComboBox<>(monthsList);
	JComboBox<Integer> secondDay = new JComboBox<>(days31);
	JComboBox<String> measureStation = new JComboBox<>(new String[] { "강남대로", "강변북로", "공항대로", "도산대로", "동작대로", "신촌로",
			"영등포로", "정릉로", "종로", "천호대로", "청계천로", "한강대로", "홍릉로", "화랑로", "강남구", "강동구", "강북구", "강서구", "관악구", "광진구", "구로구",
			"금천구", "노원구", "도봉구", "동대문구", "동장구", "마포구", "서대문구", "서초구", "성동구", "성북구", "송파구", "양천구", "영등포구", "용산구", "은평구",
			"종로구", "중구", "중랑구", "관악산(11월2일이후)", "궁동(11월2일이후)", "남산(11월2일이후)", "북한산(11월2일이후)", "세곡(11월2일이후)",
			"행주(11월2일이후)", "시흥대로(11월26일이후)" });
	JButton inquiryButton = new JButton("조회");
	// 대기오염요소
	JRadioButton NitrogenDioxide = new JRadioButton("이산화질소");
	JRadioButton OzoneConcentration = new JRadioButton("오존농도");
	JRadioButton CarbonDioxide = new JRadioButton("이산화탄소");
	JRadioButton SulfurousAcidGas = new JRadioButton("아황산가스");
	JRadioButton FineDust = new JRadioButton("미세먼지");
	JRadioButton FineParticulateMatter = new JRadioButton("초미세먼지");
	// 대기오염요소 버튼 인지
	String[] isSelectedRadioButton = new String[6];
	// 그래프 그리기
	DrawingPanel drawingPanel = new DrawingPanel();

	public InformationInquiry() {
		for (int i = 0; i < 28; i++) {
			days28[i] = i + 1;
		}
		for (int i = 0; i < 30; i++) {
			days30[i] = i + 1;
		}
		for (int i = 0; i < 31; i++) {
			days31[i] = i + 1;
		}

		firstMonth.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Integer selectInteger = (Integer) firstMonth.getSelectedItem();
				inquiryData[0] = selectInteger;
				firstDay.removeAllItems();
				if (selectInteger == 2) {
					for (int i = 0; i < 28; i++) {
						firstDay.addItem(days28[i]);
					}
				} else if (selectInteger == 4 || selectInteger == 6 || selectInteger == 9 || selectInteger == 11) {
					for (int i = 0; i < 30; i++) {
						firstDay.addItem(days30[i]);
					}
				} else if (selectInteger == 1 || selectInteger == 3 || selectInteger == 5 || selectInteger == 7
						|| selectInteger == 8 || selectInteger == 10 || selectInteger == 12) {
					for (int i = 0; i < 31; i++) {
						firstDay.addItem(days31[i]);
					}
				}
			}
		});
		firstDay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Integer selectInteger = (Integer) firstDay.getSelectedItem();
				inquiryData[1] = selectInteger;
			}
		});
		secondMonth.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Integer selectInteger = (Integer) secondMonth.getSelectedItem();
				inquiryData[2] = selectInteger;
				secondDay.removeAllItems();
				if (selectInteger == 2) {
					for (int i = 0; i < 28; i++) {
						secondDay.addItem(days28[i]);
					}
				} else if (selectInteger == 4 || selectInteger == 6 || selectInteger == 9 || selectInteger == 11) {
					for (int i = 0; i < 30; i++) {
						secondDay.addItem(days30[i]);
					}
				} else if (selectInteger == 1 || selectInteger == 3 || selectInteger == 5 || selectInteger == 7
						|| selectInteger == 8 || selectInteger == 10 || selectInteger == 12) {
					for (int i = 0; i < 31; i++) {
						secondDay.addItem(days31[i]);
					}
				}
			}
		});
		secondDay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Integer selectInteger = (Integer) secondDay.getSelectedItem();
				inquiryData[3] = selectInteger;
			}
		});

		measureStation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				inquiryMeasureData = (String) measureStation.getSelectedItem().toString();
			}
		});

		inquiryButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) { // 여기서 생기는 오류는 측정소를 누르지 않아서 생기는 오류 거기에따른 오류처리도 해줘야됨
				// 날짜 선택 오류 - 후선택 날짜가 전선택 날짜보다 이름.
				if (inquiryData[0] * 32 + inquiryData[1] > inquiryData[2] * 32 + inquiryData[3]) {
					// 오류메세지 출력
					JOptionPane.showMessageDialog(null, "날짜 선택 오류 - 후선택 날짜가 전선택 날짜보다 이릅니다.", "오류",
							JOptionPane.ERROR_MESSAGE);
				}
				// 측정소 선택 오류 - 지정된 날짜(11월 2일)보다 이른 날짜를 선택함.
				else if (inquiryMeasureData.equals("관악산(11월2일이후)") || inquiryMeasureData.equals("궁둥(11월2일이후)")
						|| inquiryMeasureData.equals("남산(11월2일이후)") || inquiryMeasureData.equals("북한산(11월2일이후)")
						|| inquiryMeasureData.equals("세곡(11월2일이후)") || inquiryMeasureData.equals("행주(11월2일이후)")) {
					if (inquiryData[0] < 11 || (inquiryData[0] == 11 && inquiryData[1] == 1)) {
						// 오류메세지 출력
						JOptionPane.showMessageDialog(null, "측정소 선택 오류 - 지정된 날짜(11월 2일)보다 이른 날짜를 선택하였습니다.", "오류",
								JOptionPane.ERROR_MESSAGE);
					}
				}
				// 측정소 선택 오류 - 지정된 날짜(11월 26일)보다 이른 날짜를 선택함.
				else if (inquiryMeasureData.equals("시흥대로(11월26일이후)")) {
					if (inquiryData[0] < 11 || (inquiryData[0] == 11 && inquiryData[1] < 26)) {
						// 오류메세지 출력
						JOptionPane.showMessageDialog(null, "측정소 선택 오류 - 지정된 날짜(11월 26일)보다 이른 날짜를 선택하였습니다.", "오류",
								JOptionPane.ERROR_MESSAGE);
					}
				} else {
					for (int j = 0; j < inquiryData.length; j++)
						System.out.println(inquiryData[j]);
					System.out.println(inquiryMeasureData);
					for (int i = 0; i < isSelectedRadioButton.length; i++) {
						System.out.println(isSelectedRadioButton[i]);
					}
				}
				new GraphicExample2();
			}
		});

		// 대기오염요소 JButton ItemEvent
		NitrogenDioxide.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				JRadioButton rb = (JRadioButton) e.getSource();
				String s = rb.getText();
				boolean isActioned = rb.isSelected();
				if (isActioned == true) {
					isSelectedRadioButton[0] = s;
				} else {
					isSelectedRadioButton[0] = null;
				}
			}
		});
		OzoneConcentration.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JRadioButton rb = (JRadioButton) e.getSource();
				String s = rb.getText();
				boolean isActioned = rb.isSelected();
				if (isActioned == true) {
					isSelectedRadioButton[1] = s;
				} else {
					isSelectedRadioButton[1] = null;
				}
			}
		});
		CarbonDioxide.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JRadioButton rb = (JRadioButton) e.getSource();
				String s = rb.getText();
				boolean isActioned = rb.isSelected();
				if (isActioned == true) {
					isSelectedRadioButton[2] = s;
				} else {
					isSelectedRadioButton[2] = null;
				}
			}
		});
		SulfurousAcidGas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JRadioButton rb = (JRadioButton) e.getSource();
				String s = rb.getText();
				boolean isActioned = rb.isSelected();
				if (isActioned == true) {
					isSelectedRadioButton[3] = s;
				} else {
					isSelectedRadioButton[3] = null;
				}
			}
		});
		FineDust.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JRadioButton rb = (JRadioButton) e.getSource();
				String s = rb.getText();
				boolean isActioned = rb.isSelected();
				if (isActioned == true) {
					isSelectedRadioButton[4] = s;
				} else {
					isSelectedRadioButton[4] = null;
				}
			}
		});
		FineParticulateMatter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JRadioButton rb = (JRadioButton) e.getSource();
				String s = rb.getText();
				boolean isActioned = rb.isSelected();
				if (isActioned == true) {
					isSelectedRadioButton[5] = s;
				} else {
					isSelectedRadioButton[5] = null;
				}
			}
		});

		frm.setSize(1100, 800);
		frm.setLayout(new BorderLayout());
		// frm.setBounds(120, 120, 200, 200);
		frm.setVisible(true);
		frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// AirPollution BorderFactory 생성
		Border AirPollutionBorder = BorderFactory.createEtchedBorder();
		AirPollutionBorder = BorderFactory.createTitledBorder(AirPollutionBorder, "대기오염요소");

		JPanel AirPollutionPanel = new JPanel();
		AirPollutionPanel.setLayout(new GridLayout(1, 6));
		AirPollutionPanel.setBorder(AirPollutionBorder);
		AirPollutionPanel.add(NitrogenDioxide);
		AirPollutionPanel.add(OzoneConcentration);
		AirPollutionPanel.add(CarbonDioxide);
		AirPollutionPanel.add(SulfurousAcidGas);
		AirPollutionPanel.add(FineDust);
		AirPollutionPanel.add(FineParticulateMatter);

		// pnlNorth 패널에 정리 월, 일, 조회소 정리

		pnlNorth.setPreferredSize(new Dimension(0, 100));
		pnlNorth.setLayout(new FlowLayout());
		pnlNorth.add(firstMonth);
		pnlNorth.add(new JLabel("월"));
		pnlNorth.add(firstDay);
		pnlNorth.add(new JLabel("일"));
		pnlNorth.add(new JLabel("부터 "));
		pnlNorth.add(secondMonth);
		pnlNorth.add(new JLabel("월"));
		pnlNorth.add(secondDay);
		pnlNorth.add(new JLabel("일"));
		pnlNorth.add(measureStation);
		pnlNorth.add(AirPollutionPanel);
		pnlNorth.add(inquiryButton);
		
		//scrollPane = new JScrollPane(drawingPanel, JScrollPane.VERTICAL_SCROLLBAR_NEVER,
		//		JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

		frm.add(pnlNorth, BorderLayout.NORTH);
	}

}
