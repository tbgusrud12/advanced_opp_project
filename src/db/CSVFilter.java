package db;

import java.io.*;
import java.nio.charset.Charset;
import java.util.*;

public class CSVFilter {
	private File inputData;
	
	public CSVFilter(String src) {
		this.inputData = new File(src);
	}
	
	public void readCSV() {
		try {
			FileReader reader = new FileReader(this.inputData);
			BufferedReader br = new BufferedReader(reader);
			
			Charset.forName("UTF-8");
			
			String line = "";

			while((line = br.readLine()) != null) {
				List<String> tmpList = new ArrayList<String>();
				String array[] = line.split(",");
				tmpList = Arrays.asList(array);
				if(tmpList.size() != 8) {
					continue;
				}

				System.out.println(tmpList);

			}
			br.close();
		}
		catch(FileNotFoundException e) {
			e.printStackTrace();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		
		return;
	}
	
}