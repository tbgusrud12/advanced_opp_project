package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class TableCreat {
    public TableCreat(){
        Connection conn = null;
        Statement stmt = null;
 
        String url = "jdbc:mysql://127.0.0.1:88/testmysql?characterEncoding=UTF-8&serverTimezone=UTC";
        String id = "root";
        String pw = "gustns50!!";
 
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
 
            conn = DriverManager.getConnection(url, id, pw);
 
            System.out.println("Successfully Connected!");
 
            //DB와 연결된 conn 객체로부터 Statement 객체 획득.
            stmt = conn.createStatement();
 
            //query 만들기
            StringBuilder sb = new StringBuilder();
            String sql = sb.append("create table if not exists Weather(")
                    .append("측정일시 int,")
                    .append("측정소명 varchar(20),")
                    .append("이산화질소농도 float,")
                    .append("오존농도 float,")
                    .append("이산화탄소농도 float,")
                    .append("아황산가스 float,")
                    .append("미세먼지 int,")
                    .append("초미세먼지 int")
                    .append(");").toString();
 
            //query문 날리기
            stmt.execute(sql);
        }
 
        catch(ClassNotFoundException e){
            e.printStackTrace();
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally{
            try{
                //자원 해제
                if(conn != null && !conn.isClosed())
                    conn.close();
            } catch(SQLException e){
                e.printStackTrace();
            }
        }
    }
    public static void main(String[] args) {
    	new TableCreat();
    	
    }
}
