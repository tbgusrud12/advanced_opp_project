package db;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnect {
	public static void DBConnectUtil(){
		Connection conn = null;

		try{
			//1. 드라이버 로딩 : mysql 드라이버 로딩
			Class.forName("com.mysql.cj.jdbc.Driver"); 
			//드라이버들이 읽히기만 하면 자동 객체가 생성되고 DriverManager에 등록된다.

			//2. mysql과 연결시키기
			String url = "jdbc:mysql://127.0.0.1:88/testmysql?characterEncoding=UTF-8&serverTimezone=UTC";

			conn = DriverManager.getConnection(url, "root", "gustns50!!");
			System.out.println("Successfully Connection!");
		}

		catch(ClassNotFoundException e){
			System.out.println("Failed because of not loading driver");
		}
		catch(SQLException e){
			System.out.println("error : " + e);
		}

		finally{
			try{
				if(conn != null && !conn.isClosed()){
					conn.close();
				}
			}

			catch(SQLException e){
				e.printStackTrace();
			}
		}
	}
}